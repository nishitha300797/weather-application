import React from "react";
import "./WeatherTable.css";
import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
function WeatherTable(props) {
  const { weather, day } = props;

  var date = new Date()
   var today = date.getDate()
  return (
    <div className="table-container">
      <TableContainer component={Paper} className="table-div">
        <Table sx={{ minWidth: 1150 }}>
          <TableHead>
            <TableRow>
              <TableCell>
                <strong>{today}</strong>
              </TableCell>
              <TableCell>
                <strong>Icon</strong>
              </TableCell>
              <TableCell>
                <strong>Temp</strong>
              </TableCell>
              <TableCell>
                <strong>Wind</strong>
              </TableCell>
              <TableCell>
                <strong>Precip.</strong>
              </TableCell>
              <TableCell>
                <strong>Cloud</strong>
              </TableCell>
              <TableCell>
                <strong>Humidity</strong>
              </TableCell>
              <TableCell>
                <strong>Pressure</strong>
              </TableCell>
            </TableRow>
          </TableHead>

          {weather?.forecast?.forecastday[day]?.hour
            .filter((item) => parseInt(item.time.slice(11, 13)) % 4 === 0)
            .map((each) => (
              // console.log("tableeach", each)
              <TableBody>
                <TableCell>{each.time.slice(11)}</TableCell>
                <TableCell>
                  <img
                    src={each.condition.icon}
                    className="image"
                    alt="pic"
                  ></img>
                </TableCell>
                <TableCell>{each.temp_c} °C</TableCell>
                <TableCell>{each.wind_kph} kmph</TableCell>
                <TableCell>{each.precip_mm} mm</TableCell>
                <TableCell>{each.cloud}%</TableCell>
                <TableCell>{each.humidity}%</TableCell>
                <TableCell>{each.pressure_mb} mb</TableCell>
              </TableBody>
            ))}
        </Table>
      </TableContainer>
    </div>
  );
}

export default WeatherTable;
