import React from "react";
import TextField from "@mui/material/TextField";
import { useState } from "react";
import axios from "axios";
import "./Home.css";
import { ToggleButtonGroup, ToggleButton } from "@mui/material";

import DailyWeather from "../DailyWeather/DailyWeather";

function Home() {
  const [searchValue, setSearchValue] = useState("");
  const [weather, setWeather] = useState([]);
  const [fetched, setFetched] = useState(false);
  const [error, setError] = useState("");
  const [day, setDay] = useState(0);

  const searchHandler = (e) => {
    setSearchValue(e.target.value);
  };
  const [temp, setTemp] = useState("F");

  const weak = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Sturday",
  ];

  const tempHandleChange = (e) => {
    setTemp(e.target.value);
    console.log(e.target.value);
  };

  const fetchDate = () => {
    const date = new Date();
    const day = date.getDay();
    setDay(day);
    setToday(weak[day]);
    console.log(date, day);
  };
  console.log(day);
  const [today, setToday] = useState("");

  const fetchData = () => {
    axios
      .get(
        `https://api.weatherapi.com/v1/forecast.json?key=0b81c00069be4d3fa7262132220408&q=${searchValue}&days=7&aqi=no&alerts=no`
      )
      .then((response) => {
        setWeather(response.data);
        fetchDate();
        setFetched(true);
      })
      .catch((error) => {
        console.log(error);
        setFetched(false);
        setError(error);
      });
  };

  const submitHandler = (e) => {
    e.preventDefault();
    fetchData();
  };
  // console.log(weather);
  console.log(weather, day, temp, today);

  return (
    <div className="main-container">
      <div className="input-container">
        <form onSubmit={submitHandler}>
          <TextField
            onChange={searchHandler}
            type="search"
            label="Please Enter City Name, US Zip Code, Canada Postal Code, UK PostCode, ip, metar, etc."
          ></TextField>
        </form>
      </div>
      {fetched === true ? (
        <div>
          <div className="forecast-container">
            <div className="location-container">
              <h4>
                {weather?.location.name} Weather Forecast{"  "}
                <span>
                  {weather?.location.region},{weather?.location.country}
                </span>
              </h4>
              <div>
                <ToggleButtonGroup color="primary">
                  <ToggleButton value="F" onClick={tempHandleChange}>
                    °F
                  </ToggleButton>
                  <ToggleButton value="C" onClick={tempHandleChange}>
                    °C
                  </ToggleButton>
                </ToggleButtonGroup>
              </div>
            </div>
            <div className="daily-container">
              <div className="today-container">
                <div className="today-temp">
                  <img
                    className="icon"
                    src={weather?.forecast?.forecastday[day].day.condition.icon}
                    alt={weather?.forecast?.forecastday[day].day.condition.text}
                  />
                  <div>
                    {temp === "F" ? (
                      <p className="today">
                        {weather?.forecast?.forecastday[day].day.avgtemp_f}{" "}
                        <span className="today">°F {today}</span>{" "}
                      </p>
                    ) : (
                      <p className="today">
                        {weather?.forecast?.forecastday[day].day.avgtemp_c}{" "}
                        <span className="today">°C {today}</span>{" "}
                      </p>
                    )}
                    <p>
                      {weather?.forecast?.forecastday[day].day.condition.text}
                    </p>
                  </div>
                </div>

                <div className="conditions">
                  <div className="wind">
                    <h6>Wind</h6>
                    <p>{weather?.current.wind_mph}Kmph</p>
                  </div>
                  <div className="precip">
                    <h6>Precip</h6>
                    <p>{weather?.current.precip_in}mm</p>
                  </div>
                  <div className="pressure">
                    <h6>Pressure</h6>
                    <p>{weather?.current.pressure_mb}mb</p>
                  </div>
                </div>
              </div>
              <div className="weak-container">
                {/* {weather?.forecast.forecastday.splice(0,day,).map((each)=> (
             
          ))} */}
                {weather?.forecast?.forecastday.map((each) => (
                  <div className="box-container">
                    <img
                      className="icon"
                      src={each.day.condition.icon}
                      alt={each.day.condition.text}
                    />
                    {temp === "F" ? (
                      <p>{each.day.avgtemp_f} °F </p>
                    ) : (
                      <p>{each.day.avgtemp_c} °C</p>
                    )}

                    <p> {each.day.condition.text}</p>
                  </div>
                ))}
              </div>
            </div>
          </div>

          <div className="today-weather-container">
            <h4>Today's Weather in {weather?.location.name}</h4>
            <div className="forecast">
              <div className="temp">
                <div>
                  <p>
                    Sunrise {weather?.forecast?.forecastday[day].astro.sunrise}
                  </p>
                  <p>
                    Sunset {weather?.forecast?.forecastday[day].astro.sunset}
                  </p>
                </div>
                <div>
                  <p>
                    Moonrise{" "}
                    {weather?.forecast?.forecastday[day].astro.moonrise}
                  </p>
                  <p>
                    Moonrise {weather?.forecast?.forecastday[day].astro.moonset}
                  </p>
                </div>
              </div>
              <div className="condition">
                <p>
                  Max <br />
                  {weather?.forecast?.forecastday[day].day.maxtemp_c} °C
                </p>
                <p>
                  Min <br />
                  {weather?.forecast?.forecastday[day].day.mintemp_c} °C
                </p>
                <p>
                  Avg
                  <br />
                  {weather?.forecast?.forecastday[day].day.avgtemp_c}pm
                </p>
                <p>
                  Precip. <br />
                  {weather?.forecast?.forecastday[day].day.totalprecip_mm}mm
                </p>

                <p>
                  Max Wind <br />
                  {weather?.forecast?.forecastday[day].day.maxwind_mph}Kph
                </p>
              </div>
            </div>
          </div>

          <DailyWeather weather={weather} searchValue={searchValue} />
        </div>
      ) : (
        <div className="error-container">
          <h4>{error?.response?.data?.error?.message} </h4>
          <p>Please Enter the City Name</p>
        </div>
      )}
    </div>
  );
}

export default Home;
