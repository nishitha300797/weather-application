// import React, { useState } from "react";
// import Chart from "react-apexcharts";
// import axios from "axios";
// import "./WeatherChart.css";

// function WeatherChart(props) {
//   const { searchValue, day } = props;
//   const [options, setOptions] = useState({
//     chart: {
//       id: "basic-bar",
//     },
//     xaxis: {
//       categories: [],
//     },
//   });
//   const [series, setSeries] = useState({
//     name: "series-1",
//     data: [],
//   });
//   const fetch = () => {
//     const temp = [];
//     const hrs = [];
//     axios
//       .get(
//         `https://api.weatherapi.com/v1/forecast.json?key=0b81c00069be4d3fa7262132220408&q=${searchValue}&days=7&aqi=no&alerts=no`
//       )
//       .then((response) => {
//         response.data.forecast.forecastday[day].hour.map((each) => {
//           hrs.push(each.time.slice(11));
//           temp.push(each.temp_c);
//           setOptions({
//             chart: {
//               id: "basic-bar",
//             },
//             xaxis: {
//               categories: hrs.slice(0, 7),
//             },
//           });
//           setSeries([
//             {
//               name: "series-1",
//               data: temp.slice(0, 7),
//             },
//           ]);
//         });
//         // console.log(hrs, temp);
//       })
//       .catch((e) => console.log(e));
//   };

//   fetch();

//   return (
//     <div className="chart-container">
//       <Chart options={options} series={series} type="line" width={1000} />
//     </div>
//   );
// }

// export default WeatherChart;

import React, { useEffect, useState } from "react";
import './WeatherChart.css'
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  Title,
  Tooltip,
  Legend,
  LineElement,
  PointElement
} from "chart.js";
import { Line } from "react-chartjs-2";
import axios from "axios";
ChartJS.register(
  CategoryScale,
  LinearScale,
  LineElement,
  Tooltip,
  Legend,
  Title,
  PointElement
);



const options = {
  type: 'line',
  indexAxis: "x",
  elements: {
    line: {
      borderWidth: 2,
    },
    responsive: true,
    plugins: {
      legend: {
        position: "right",
      },
    },
  },
};

const WeatherChart = (props) => {
  const { searchValue, day } = props;
  const [data, setData] = useState({
    labels:[],
    datasets:[
      {
      label: 'Dataset ',
      data:[] ,
      borderColor:'lightblue',
      backgroundColor:'lightblue',
      }
    ] 
  })

  const fetchData = () => {
    const temp = [];
    const hrs = [];
    axios.get(
      `https://api.weatherapi.com/v1/forecast.json?key=0b81c00069be4d3fa7262132220408&q=${searchValue}&days=7&aqi=no&alerts=no`
    ).then((response)=>{
      // console.log(response)
      response.data.forecast.forecastday[day].hour.map((each) => {
                hrs.push(each.time.slice(11));
               temp.push(each.temp_c);
      
      })
      setData(
        {
          labels:hrs,
          datasets:[
            {
            label: 'Dataset ',
            data:temp ,
            borderColor:'lavender',
            backgroundColor:'lavender'
            }
          ] 
        }
      )
    })
    .catch((error)=>{
      console.log(error)
    })
    // console.log(hrs,temp)
  };

  fetchData();
 

  return (<div className='chart-container'>
    <Line options={options} data={data} width={1000} height={600}/>
  </div>)
};

export default WeatherChart;
