import React, { useState } from "react";
import './DailyWeather.css'
import { Box, Tab, ToggleButtonGroup, ToggleButton } from "@mui/material";
import { TabContext, TabList, TabPanel } from "@mui/lab";
import SsidChartIcon from "@mui/icons-material/SsidChart";
import GridViewIcon from "@mui/icons-material/GridView";

import WeatherTable from "../WeatherTable/WeatherTable";
import WeatherChart from "../WeatherChart/WeatherChart";

function DailyWeather(props) {
  const { weather, searchValue } = props;
  const [toggleDay, setToggleDay] = React.useState(0);
  const [value, setValue] = React.useState("1");
  const [day, setDay] = useState(0);

  const dayToggleBtnHandleChange = (event, newAlignment) => {
    setToggleDay(parseInt(newAlignment));
  };
  const dayHandleChange = (e) => {
    setDay(e.target.value);
  };
  const handleChanges = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <div className="buttons-container">
      <div className="week-button">
        <ToggleButtonGroup
          color="primary"
          exclusive
          value={toggleDay}
          onChange={dayToggleBtnHandleChange}
        >
          <ToggleButton value={0} onClick={dayHandleChange}>
            monday
          </ToggleButton>
          <ToggleButton value={1} onClick={dayHandleChange}>
            Tuesday
          </ToggleButton>
          <ToggleButton value={2} onClick={dayHandleChange}>
            Wednesday
          </ToggleButton>
          <ToggleButton value={3} onClick={dayHandleChange}>
            Thursday
          </ToggleButton>
          <ToggleButton value={4} onClick={dayHandleChange}>
            Friday
          </ToggleButton>
          <ToggleButton value={5} onClick={dayHandleChange}>
            Saturday
          </ToggleButton>
          <ToggleButton value={6} onClick={dayHandleChange}>
            Sunday
          </ToggleButton>
        </ToggleButtonGroup>
      </div>
      <div className="chart-table-buttons">
        <Box>
          <TabContext value={value}>
            <Box>
              <TabList
                onChange={handleChanges}
                aria-label="lab API tabs example"
                color="primary"
              >
                <Tab icon={<GridViewIcon />} className='table' value="1" />
                <Tab icon={<SsidChartIcon />} className="chart" value="2" />
              </TabList>
            </Box>
            <TabPanel value="1">
              <WeatherTable weather={weather} day={day} />
            </TabPanel>
            <TabPanel value="2">
              <WeatherChart searchValue={searchValue} day={day} />
            </TabPanel>
          </TabContext>
        </Box>
      </div>
    </div>
  );
}

export default DailyWeather;
